import sys
from PySide6.QtGui     import *
from PySide6.QtCore    import *
from PySide6.QtWidgets import *
import pyautogui
import socket
import ctypes
from pathlib import Path

class Widget(QWidget):
    
    def __init__(self):
        super().__init__()
        
        self.resize(ctypes.windll.user32.GetSystemMetrics(0), ctypes.windll.user32.GetSystemMetrics(1))

        button = QPushButton(self)
        button.setText("Выйти")
        button.setStyleSheet("background-color: yellow;")
        button.move(0, 0)

        button.clicked.connect(self.exit)

        ipText = socket.gethostbyname(socket.gethostname())
        msgBox = QMessageBox(self)
        msgBox.setIcon(QMessageBox.Information)
        msgBox.setText(ipText)
        msgBox.setWindowTitle("IP компьютера")
        msgBox.setStandardButtons(QMessageBox.Ok)
        msgBox.exec()

        #self.penList.append(QPen(Qt.black))
        
        self.setStyleSheet("background:transparent;")
        self.setAttribute(Qt.WA_TranslucentBackground)
        self.setWindowFlag(Qt.FramelessWindowHint)
        self.setWindowFlag(Qt.WindowStaysOnTopHint)
        self.setWindowFlag(Qt.Tool)
        #self.show()

    def exit(self):
        QApplication.quit()

    updated = True
    endLine = True
    lineList = []
    penList = []
    textList = []
    curLine = 0
    
    def UpdateLine(self):     
        self.updated = True
        self.update()
    
    def EndLine(self):
        self.endLine = True
        if(len(self.lineList) <= self.curLine):
            self.lineList.append([])
        self.PenCheckOrSetDefault()
        self.curLine += 1
    
    def SetPenColor(self, r, g, b, a):
        if(len(self.penList) <= self.curLine):
            if self.curLine == 0:
                pen = QPen(QColor(r, g, b, a))
                pen.setWidth(5)
                pen.setCapStyle(Qt.RoundCap)
                self.penList.append(pen)
            else:
                pen = QPen()
                pen.setColor(QColor(r, g, b, a))
                pen.setWidth(self.penList[self.curLine - 1].width())
                pen.setCapStyle(Qt.RoundCap)
                self.penList.append(pen)
        else:
            self.penList[self.curLine].setColor(QColor(r, g, b, a))

    def SetPenWidth(self, width):
        if(len(self.penList) <= self.curLine):
            if self.curLine == 0:
                pen = QPen(Qt.black)
                pen.setWidth(width)
                pen.setCapStyle(Qt.RoundCap)
                self.penList.append(pen)
            else:
                pen = QPen()
                pen.setColor(self.penList[self.curLine - 1].color())
                pen.setWidth(width)
                pen.setCapStyle(Qt.RoundCap)
                self.penList.append(pen)
        else:
            self.penList[self.curLine].setWidth(width)

    def PenCheckOrSetDefault(self):
        if(len(self.penList) <= self.curLine):
            if self.curLine == 0:
                pen = QPen(Qt.black)
                pen.setWidth(5)
                pen.setCapStyle(Qt.RoundCap)
                self.penList.append(pen)
            else:
                pen = QPen()
                pen.setColor(self.penList[self.curLine - 1].color())
                pen.setWidth(self.penList[self.curLine - 1].width())
                pen.setCapStyle(Qt.RoundCap)
                self.penList.append(pen)

    def ProceedLine(self, points):
        if self.endLine:
            self.lineList.append([])
            self.PenCheckOrSetDefault()
            self.endLine = False
        #print(self.lineList)
        self.lineList[self.curLine].extend(points)
        self.update()

    def DrawText(self, textParams):
        self.PenCheckOrSetDefault()
        pen = QPen()
        pen.setWidth(self.penList[self.curLine].width())
        pen.setColor(self.penList[self.curLine].color())
        self.textList.append([textParams, pen])

    def ClearCanvas(self):
        self.lineList = []
        self.textList = []
        self.PenCheckOrSetDefault()
        pen = self.penList[self.curLine]
        self.penList = []
        self.penList.append(pen)
        self.curLine = 0
        self.updated = False
        self.endLine = True
        self.update()

    def paintEvent(self, event):
        if True:
            painter = QPainter()
            for i in range(len(self.lineList)):
                painter.begin(self)
                path = QPainterPath()
                l = self.lineList[i]
                j = 0
                count = len(l)
                while j < count:
                    if j == 0:
                        path.moveTo(l[j].x(), l[j].y())
                    else:
                        path.lineTo(l[j].x(), l[j].y())
                        path.moveTo(l[j].x(), l[j].y())
                    j += 1
                painter = QPainter()
                painter.begin(self)
                pen = self.penList[i]
                painter.setPen(pen)
                painter.drawPath(path)
                painter.end()
            
            for t in self.textList:
                textParams = t[0]
                pen = t[1]

                painter = QPainter()
                painter.begin(self)
                painter.setFont(QFont("Arial", textParams[2]))
                painter.setPen(pen) 
                painter.drawText(textParams[0], textParams[1], textParams[3])
                painter.end()
                
            self.updated = False

app = QApplication(sys.argv)
ex = Widget()

def drawLine(tenRelativePoints):
    listPoint = []
    for p in tenRelativePoints:
        listPoint.append(QPoint(p[0] * ctypes.windll.user32.GetSystemMetrics(0), p[1] * ctypes.windll.user32.GetSystemMetrics(1)))
    #print(listPoint)
    ex.ProceedLine(listPoint)

def finishLine(data):
    ex.EndLine()

def cleanCanvas(data):
    ex.ClearCanvas()

def changeBrushParameters(brushParams):
    ex.SetPenColor(brushParams[1], brushParams[2], brushParams[3], brushParams[4])
    ex.SetPenWidth(brushParams[0])

screenCount = 1
def saveSlide(data):
    Path("./screen").mkdir(parents=True, exist_ok=True)
    global screenCount
    myScreenshot = pyautogui.screenshot()
    myScreenshot.save(r'./screen/sreen' + str(screenCount) + '.png')
    #myScreenshot.save(r'./screen/screen1' + str(screenCount) + '.png')
    screenCount += 1

def drawText(textParams):
    ex.DrawText(textParams)

def runApp():
    app.setStyle("Fusion")
    ex.show()
    sys.exit(app.exec_())

if __name__ == '__main__':
    app.setStyle("Fusion")
    print(QStyleFactory.keys())
    ex.ProceedLine([])
    ex.SetPenColor(0, 0, 255, 255)
    ex.DrawText([100, 100, 30, "Hello"])
    ex.SetPenColor(0, 255, 255, 255)
    ex.DrawText([100, 300, 40, "Hello22"])
    ex.show()
    sys.exit(app.exec_())