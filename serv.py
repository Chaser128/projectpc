import asyncio
import websockets
import json
from Presenter import *
import ctypes
import canvas
import threading
import sys

screenRatio = ctypes.windll.user32.GetSystemMetrics(0) / ctypes.windll.user32.GetSystemMetrics(1)

async def scriptServerWork(websocket, path):
    global screenRatio
    client = websocket
    await client.send("{\"Type\":\"Ratio\", \"Data\":" + str(screenRatio) + "}")
    async for message in websocket:
        dictData = json.loads(message)
        print(dictData)
        manipulate(dictData)


def startServer():
    asyncio.set_event_loop(asyncio.new_event_loop())
    asyncio.get_event_loop().run_until_complete(
        websockets.serve(scriptServerWork, '', 8080))
    asyncio.get_event_loop().run_forever()

threading.Thread(target=startServer, daemon=True).start()

async def testFunc(websocket, path):
    await websocket.recv()
    print(f"< {name}")

    greeting = f"Hello {name}!"

    await websocket.send(greeting)
    print(f"> {greeting}")

canvas.runApp()

sys.exit()