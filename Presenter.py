import json
import keyboard
from canvas import *

def switching(data):
    if data == "Next":
        keyboard.goNextSlide()
    elif data == "Previous":
        keyboard.goPreviousSlide()
    else:
        raise Exception("Impossible situation")        

actions={
    "SwitchSlide": switching,
    "DrawLine": drawLine,
    "EndLine": finishLine,
    "CleanCanvas": cleanCanvas,
    "ChangeBrushParameters": changeBrushParameters,
    "SaveSlide": saveSlide,
    "DrawText": drawText
}

def manipulate(deserializedMessage):
    print(deserializedMessage)
    type = deserializedMessage["Type"]
    print(type)
    data = deserializedMessage["Data"]
    actions[type](data)    

    