import pyautogui
import threading

lk = threading.Lock()

nextSlideFlag=True
def goNextSlide():
    global nextSlideFlag
    lk.locked
    if nextSlideFlag == False:
        return
    nextSlideFlag=False        
    lk.release                
    pyautogui.press("right", presses=1)
    nextSlideFlag=True 

previousSlideFlag=True
def goPreviousSlide():
    global previousSlideFlag
    lk.locked
    if previousSlideFlag == False:
        return
    previousSlideFlag=False        
    lk.release                
    pyautogui.press("left", presses=1)
    previousSlideFlag=True 